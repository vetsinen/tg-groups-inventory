### python script which grabs all your tg groups to csv-file

you need local python intalled on computer to run this script

after cloning repository you need to install dependencies by running in this folder `pip install -r requirements.txt`

to get csv file with information about your groups you need to run
`python grabber.py`

on first launch you need to autorize this app to your telegram by entering your phone number and confirmation code from your main telegram client
your data is stored in session file and you don't need to repeat authorization
