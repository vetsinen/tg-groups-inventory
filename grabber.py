from telethon.sync import TelegramClient
import csv

api_id: int = 14535551 ; api_hash: str = 'ee049ec9130de53ec5336fe819e49365'
data = [
    ['id', 'title', 'groupname', 'population'],
]

def write_csv():
    filename = 'groups.csv'
    with open(filename, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(data)
    print("CSV file created successfully.")

def get_groups():
    i=0
    with TelegramClient('helvetian', api_id, api_hash) as client:
        for dialog in client.iter_dialogs():
            if (dialog.is_group or dialog.is_channel) and not dialog.is_user:
                i = i + 1
                entity = client.get_entity(dialog.entity.id)
                # print(vars(entity))
                try:
                    members = len(client.get_participants(entity))
                except:
                    members=-1
                data.append([entity.id,entity.title,entity.username if hasattr(entity,"username") else None, members])

if __name__=='__main__':
    get_groups()
    write_csv()